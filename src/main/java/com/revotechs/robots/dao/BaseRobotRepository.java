package com.revotechs.robots.dao;

import com.revotechs.robots.entity.Robot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BaseRobotRepository<T extends Robot> extends JpaRepository<T, UUID> {


}
