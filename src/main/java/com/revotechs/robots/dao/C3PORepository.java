package com.revotechs.robots.dao;

import com.revotechs.robots.entity.C3PO;
import org.springframework.stereotype.Repository;

@Repository
public interface C3PORepository extends BaseRobotRepository<C3PO> {

}
