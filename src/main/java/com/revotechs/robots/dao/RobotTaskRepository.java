package com.revotechs.robots.dao;

import com.revotechs.robots.entity.RobotTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface RobotTaskRepository extends JpaRepository<RobotTask, Long> {

    RobotTask findFirstByRobotId(UUID Id);

    List <RobotTask> findAllByRobotId(UUID id);

}
