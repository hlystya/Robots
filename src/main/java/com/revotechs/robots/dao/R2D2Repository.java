package com.revotechs.robots.dao;

import com.revotechs.robots.entity.R2D2;
import org.springframework.stereotype.Repository;

@Repository
public interface R2D2Repository extends BaseRobotRepository<R2D2> {

}
