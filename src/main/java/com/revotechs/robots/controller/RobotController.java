package com.revotechs.robots.controller;

import com.revotechs.robots.entity.*;
import com.revotechs.robots.services.C3POService;
import com.revotechs.robots.services.R2D2Service;
import com.revotechs.robots.services.RobotTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/robots")
public class RobotController {

    private final R2D2Service r2D2Service;
    private final C3POService c3POService;
    private final RobotTaskService robotTaskService;
    private List<String> tasks = Arrays.asList("MOVE", "TALK", "REPAIR", "KILL_SELF", "KILL_ALL_PEOPLE", "CRAFT_FUNNY", "KILL_EACH_OTHER");

    @Autowired
    public RobotController(R2D2Service r2D2Service, C3POService c3POService, RobotTaskService robotTaskService) {
        this.r2D2Service = r2D2Service;
        this.c3POService = c3POService;
        this.robotTaskService = robotTaskService;
    }

    @GetMapping("")
    public String showRobotsPage(Model model, Pageable pageable) {
        Page<R2D2> r2D2s = r2D2Service.findAll(pageable);
        Page<C3PO> c3POs = c3POService.findAll(pageable);
        List<Robot> robots = new ArrayList<>();
        robots.addAll(r2D2s.getContent());
        robots.addAll(c3POs.getContent());
        TaskForm taskForm = new TaskForm();
        model.addAttribute("taskForm", taskForm);
        model.addAttribute("robots", robots);
        model.addAttribute("tasks", tasks);
        return "robots";
    }


    @GetMapping("/newRobot")
    public String showNewRobotPage(Model model) {
        RobotForm robotForm = new RobotForm();
        model.addAttribute("robotForm", robotForm);
        return "newRobot";
    }

    @PostMapping("/newRobot")
    public String saveRobot(@ModelAttribute("robotForm") RobotForm robotForm) {
        String name = robotForm.getName();
        String type = robotForm.getType();
        if (type.equals("C3PO") && !StringUtils.isEmpty(name)) {
            c3POService.createNewRobot(name);
        }
        if (type.equals("R2D2") && !StringUtils.isEmpty(name)) {
            r2D2Service.createNewRobot(name);
        }
        return "redirect:/robots";
    }


    @PostMapping("")
    public String createTask(@ModelAttribute("taskForm") TaskForm taskForm) {
        List<UUID> robotsUuid = taskForm.getRobotsId();
        String task = taskForm.getTask();
        List<Robot> robots = new ArrayList<>();
        robots.addAll(r2D2Service.findRobotsList());
        robots.addAll(c3POService.findRobotsList());
        robotTaskService.createTask(robotsUuid, task, robots);
        return "redirect:/robots";
    }


}
