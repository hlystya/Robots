package com.revotechs.robots.controller.rest;

import com.revotechs.robots.entity.*;
import com.revotechs.robots.services.C3POService;
import com.revotechs.robots.services.R2D2Service;
import com.revotechs.robots.services.RobotTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/robots/")
public class RestRobotController {
    private List<String> tasks = Arrays.asList("MOVE", "TALK", "REPAIR", "KILL_SELF", "KILL_ALL_PEOPLE", "CRAFT_FUNNY", "KILL_EACH_OTHER");

    private final R2D2Service r2D2Service;
    private final C3POService c3POService;
    private final RobotTaskService robotTaskService;

    @Autowired
    public RestRobotController(R2D2Service r2D2Service, C3POService c3POService, RobotTaskService robotTaskService) {
        this.r2D2Service = r2D2Service;
        this.c3POService = c3POService;
        this.robotTaskService = robotTaskService;
    }


    @GetMapping("")
    public ResponseEntity<List<Robot>> getAllRobots(Pageable pageable) {
        Page<R2D2> r2D2s = r2D2Service.findAll(pageable);
        Page<C3PO> c3POs = c3POService.findAll(pageable);
        List<Robot> robots = new ArrayList<>();
        robots.addAll(r2D2s.getContent());
        robots.addAll(c3POs.getContent());
        return (new ResponseEntity<>(robots, HttpStatus.OK));
    }


    @GetMapping("/tasks")
    public ResponseEntity<List<String>> getAllTasks() {
        return (new ResponseEntity<>(tasks, HttpStatus.OK));
    }

    @PostMapping("/newRobot")
    ResponseEntity saveRobot(@RequestBody RobotForm robotForm) {
        String name = robotForm.getName();
        String type = robotForm.getType();
        if (type.equals("C3PO") && !StringUtils.isEmpty(name)) {
            c3POService.createNewRobot(name);
        }
        if (type.equals("R2D2") && !StringUtils.isEmpty(name)) {
            r2D2Service.createNewRobot(name);
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @PostMapping("/newTask")
    public ResponseEntity createTask(@RequestBody TaskForm taskForm) {
        List<UUID> robotsUuid = taskForm.getRobotsId();
        String task = taskForm.getTask();
        List<Robot> robots = new ArrayList<>();
        robots.addAll(r2D2Service.findRobotsList());
        robots.addAll(c3POService.findRobotsList());
        robotTaskService.createTask(robotsUuid, task, robots);
        return new ResponseEntity(HttpStatus.OK);
    }



}
