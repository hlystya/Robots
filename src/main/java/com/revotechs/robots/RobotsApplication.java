package com.revotechs.robots;

import com.github.javafaker.Faker;
import com.revotechs.robots.services.C3POService;
import com.revotechs.robots.services.R2D2Service;
import com.revotechs.robots.services.TrackerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotsApplication {

    private static R2D2Service r2D2Service;
    private static C3POService c3POService;
    private static TrackerService trackerService;

    @Autowired
    public RobotsApplication(R2D2Service r2D2Service, C3POService c3POService, TrackerService trackerService) {
        RobotsApplication.r2D2Service = r2D2Service;
        RobotsApplication.c3POService = c3POService;
        RobotsApplication.trackerService = trackerService;
    }

    public static void main(String[] args) {
        SpringApplication.run(RobotsApplication.class, args);
        // Faker генерирует имена
        Faker faker = new Faker();
        //создаем роботов
        for (int i = 0; i < 4; i++) {
            r2D2Service.createNewRobot("R2D2 " + faker.name().name());
        }
        for (int i = 0; i < 4; i++) {
            c3POService.createNewRobot("C3PO " + faker.name().name());
        }

        //отслеживаем нехватку роботов
        trackerService.trackActivity();

    }
}
