package com.revotechs.robots.services;

import com.revotechs.robots.dao.RobotTaskRepository;
import com.revotechs.robots.entity.Robot;
import com.revotechs.robots.entity.RobotTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class RobotTaskService {
    private final RobotTaskRepository robotTaskRepository;

    @Autowired
    public RobotTaskService(RobotTaskRepository robotTaskRepository) {
        this.robotTaskRepository = robotTaskRepository;
    }

    @Transactional
    public void save(RobotTask task) {
        robotTaskRepository.save(task);
    }


    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<RobotTask> findAll(Pageable pageable) {
        return robotTaskRepository.findAll(pageable);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RobotTask findOne(Long id) {
        return robotTaskRepository.findOne(id);
    }

    public RobotTask findFirstByRobotId(UUID id) {
        return robotTaskRepository.findFirstByRobotId(id);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<RobotTask> findAllByRobotId(UUID id) {
        return robotTaskRepository.findAllByRobotId(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(RobotTask robotTask) {
        if (robotTaskRepository.findOne(robotTask.getId()) != null) {
            robotTaskRepository.delete(robotTask.getId());
        }
    }

    public void createTask(List<UUID> robotsUuid, String task, List<Robot> robots) {
        if (robotsUuid.isEmpty()) {
            for (Robot robot : robots) {
                RobotTask robotTask = new RobotTask();
                robotTask.setTask(task);
                robotTask.setRobotId(robot.getId());
                save(robotTask);

            }
        } else {
            for (UUID uuid : robotsUuid) {
                RobotTask robotTask = new RobotTask();
                robotTask.setTask(task);
                robotTask.setRobotId(uuid);
                save(robotTask);
            }
        }
    }

}
