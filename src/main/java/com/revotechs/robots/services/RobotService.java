package com.revotechs.robots.services;

import com.revotechs.robots.dao.BaseRobotRepository;
import com.revotechs.robots.entity.Robot;
import com.revotechs.robots.entity.RobotTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Thread.sleep;

@Service
@Transactional
public abstract class RobotService<T extends Robot> {
    private BaseRobotRepository<T> baseRobotRepository;
    private final RobotTaskService robotTaskService;
    private List<T> deadRobots = new ArrayList<>();

    @Autowired
    protected RobotService(BaseRobotRepository<T> baseRobotRepository, RobotTaskService robotTaskService) {
        this.baseRobotRepository = baseRobotRepository;
        this.robotTaskService = robotTaskService;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public T findById(UUID id) {
        return baseRobotRepository.getOne(id);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Page<T> findAll(Pageable pageable) {
        return baseRobotRepository.findAll(pageable);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<T> findRobotsList() {
        return baseRobotRepository.findAll();
    }


    public List<T> getDeadRobots() {
        return deadRobots;
    }

    public void save(T robot) {
        baseRobotRepository.save(robot);
        runRobotInNewThread(robot);
    }

    /**
     * В этом методе создается отдельный поток для робота.
     **/
    private void runRobotInNewThread(T robot) {
        Thread thread = new Thread(() -> {
            while (baseRobotRepository.getOne(robot.getId()) != null) {
                findTask(robot);
            }
        });
        thread.start();
    }


    /**
     * Пока робот жив, он идет в очередь заданий, берет задание, выполняет его (засыпает), удаяет из очереди
     * Если задание означает "Убей себя", память о нем заносится на доску почета, а он удаляется
     */

    private void findTask(T robot) {
        RobotTask task = robotTaskService.findFirstByRobotId(robot.getId());


        if (task != null && !StringUtils.isEmpty(task.getTask()) && !task.getTask().equals("KILL_SELF")) {
            execute(robot, task.getTask());
            robotTaskService.delete(task);
            baseRobotRepository.save(robot);
        } else if (task != null && !StringUtils.isEmpty(task.getTask()) && task.getTask().equals("KILL_SELF")) {
            deadRobots.add(robot);
            baseRobotRepository.delete(robot);
            robotTaskService.delete(task);
        }
        try {
            sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            baseRobotRepository.delete(robot);
        }
    }


    private void execute(Robot robot, String task) {
        String message;
        switch (task) {
            case "MOVE": {
                message = robot.move();
                break;
            }
            case "TALK": {
                message = robot.talk();
                break;
            }
            case "REPAIR": {
                message = robot.repair();
                break;
            }
            case "CRAFT_FUNNY": {
                message = robot.craft();
                break;
            }
            case "KILL_ALL_PEOPLE": {
                message = robot.killAllPeople();
                break;
            }
            case "KILL_EACH_OTHER": {
                message = robot.killEachOther();
                break;
            }
            default:
                message = task;
        }
        robot.setTask(message);
    }


    private void removeDeadRobot(int index) {
        deadRobots.remove(index);
    }

    public void resurrectRobot() {
        if (!getDeadRobots().isEmpty()) {
            T robot = getDeadRobots().get(0);
            save(robot);
            removeDeadRobot(0);
        }
    }

}
