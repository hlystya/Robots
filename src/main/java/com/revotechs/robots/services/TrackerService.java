package com.revotechs.robots.services;

import com.github.javafaker.Faker;
import com.revotechs.robots.entity.C3PO;
import com.revotechs.robots.entity.R2D2;
import com.revotechs.robots.entity.RobotTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.Thread.sleep;

@Service
public class TrackerService {
    private final RobotTaskService taskService;
    private final R2D2Service r2D2Service;
    private final C3POService c3POService;


    @Autowired
    public TrackerService(RobotTaskService taskService, R2D2Service r2D2Service, C3POService c3POService) {
        this.taskService = taskService;
        this.r2D2Service = r2D2Service;
        this.c3POService = c3POService;
    }


    /**
     * Этот метод работает на протяжении всего цикла приложения.
     * задача заключается в отслеживании убитых роботов, и их восстановлении.
     * Так же в этом  методе создаются новые роботы, если существующие не справляются с заданиями
     **/
    public void trackActivity() {
        Thread thread = new Thread(() -> {
            while (true) {

                if (!c3POService.getDeadRobots().isEmpty()) {
                    c3POService.resurrectRobot();
                }
                if (!r2D2Service.getDeadRobots().isEmpty()) {
                    r2D2Service.resurrectRobot();
                }

                Faker faker = new Faker();
                List<C3PO> c3POs = c3POService.findRobotsList();
                List<R2D2> r2D2s = r2D2Service.findRobotsList();
                List<RobotTask> freeTasks = taskService.findAllByRobotId(null);

                if (freeTasks.size() > 10 && c3POs.size() > r2D2s.size()) {
                    r2D2Service.createNewRobot("R2D2" + faker.name().name());
                } else if ((freeTasks.size() > 10 && r2D2s.size() > c3POs.size())) {
                    c3POService.createNewRobot("C3PO" + faker.name().name());
                }
                try {
                    sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }


}
