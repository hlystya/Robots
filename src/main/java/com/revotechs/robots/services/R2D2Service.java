package com.revotechs.robots.services;

import com.revotechs.robots.dao.R2D2Repository;
import com.revotechs.robots.entity.R2D2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class R2D2Service extends RobotService<R2D2> {
    private final R2D2Repository r2D2Repository;
    private final RobotTaskService robotTaskService;

    @Autowired
    public R2D2Service(R2D2Repository r2D2Repository, RobotTaskService robotTaskService) {
        super(r2D2Repository, robotTaskService);
        this.r2D2Repository = r2D2Repository;
        this.robotTaskService = robotTaskService;
    }


    public void createNewRobot(String name) {
        R2D2 newR2D2 = new R2D2();
        newR2D2.setEnergy(100);
        newR2D2.setStrength((int) (Math.random() * 256));
        newR2D2.setAgility((int) (Math.random() * 256));
        newR2D2.setIntellect((int) (Math.random() * 256));
        newR2D2.setName(name);
        save(newR2D2);
    }


}
