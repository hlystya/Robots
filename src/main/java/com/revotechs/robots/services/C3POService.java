package com.revotechs.robots.services;

import com.revotechs.robots.dao.C3PORepository;
import com.revotechs.robots.entity.C3PO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class C3POService extends RobotService<C3PO> {
    private final C3PORepository c3PORepository;
    private final RobotTaskService robotTaskService;

    @Autowired
    public C3POService(C3PORepository c3PORepository, RobotTaskService robotTaskService) {
        super(c3PORepository, robotTaskService);
        this.c3PORepository = c3PORepository;
        this.robotTaskService = robotTaskService;
    }


    public void createNewRobot(String name) {
        C3PO newC3po = new C3PO();
        newC3po.setEnergy(100);
        newC3po.setStrength((int) (Math.random() * 256));
        newC3po.setAgility((int) (Math.random() * 256));
        newC3po.setIntellect((int) (Math.random() * 256));
        newC3po.setName(name);
        save(newC3po);
    }


}
