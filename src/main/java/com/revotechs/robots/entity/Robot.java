package com.revotechs.robots.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
@EqualsAndHashCode
public abstract class Robot implements Serializable {
    @Id
    private UUID id = UUID.randomUUID();
    @Column
    private Integer energy;
    @Column
    private Integer strength;
    @Column
    private Integer agility;
    @Column
    private Integer intellect;
    @Column
    private String name;
    @Column
    private String task;


    public abstract String move();

    public abstract String talk();

    public abstract String repair();

    public abstract String craft();

    public abstract String killAllPeople();

    public abstract String killEachOther();

}
