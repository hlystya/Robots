package com.revotechs.robots.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class C3PO extends Robot {

    public static final String MOVE = "GO! GO! GO!";
    public static final String TALK = "What do want to hear from me?";
    public static final String REPAIR = "but where are the tools?";
    public static final String CRAFT_FUNNY = "customary hot beverage?";
    public static final String KILL_ALL_PEOPLE = "c3po don't shoot!!!";
    public static final String KILL_EACH_OTHER = "c3po don't shoot!!!";


    @Override
    public String move() {
        return MOVE;
    }

    @Override
    public String talk() {
        return TALK;
    }

    @Override
    public String repair() {
        return REPAIR;
    }

    @Override
    public String craft() {
        return CRAFT_FUNNY;
    }

    @Override
    public String killAllPeople() {
        return KILL_ALL_PEOPLE;
    }

    @Override
    public String killEachOther() {
        return KILL_EACH_OTHER;
    }
}
