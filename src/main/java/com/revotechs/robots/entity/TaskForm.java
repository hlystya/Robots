package com.revotechs.robots.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskForm {
    private List<UUID> robotsId = new ArrayList<>();
    private String task;
}
