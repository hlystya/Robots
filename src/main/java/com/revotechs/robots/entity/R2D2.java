package com.revotechs.robots.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class R2D2 extends Robot {

    public static final String MOVE = "bowl along";
    public static final String TALK = "pew pew pew";
    public static final String REPAIR = "READY!";
    public static final String CRAFT_FUNNY = "I’m reading a great book about Force levitation… I can’t put it down.";
    public static final String KILL_ALL = "how great is that?";
    public static final String KILL_EACH_OTHER = "R2D2 don't killing friends!";


    @Override
    public String move() {
        return MOVE;
    }

    @Override
    public String talk() {
        return TALK;
    }

    @Override
    public String repair() {
        return REPAIR;
    }

    @Override
    public String craft() {
        return CRAFT_FUNNY;
    }

    @Override
    public String killAllPeople() {
        return KILL_EACH_OTHER;
    }

    @Override
    public String killEachOther() {
        return KILL_ALL;
    }
}
