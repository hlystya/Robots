setInterval(function () {
    $.ajax({
        method: "GET",
        url: "http://localhost/api/robots/",
        success: function (data) {
            $("tbody").empty();
            $.each(data, function (key, value) {
                var id = value.id;
                var strength = value.strength;
                var agility = value.agility;
                var intellect = value.intellect;
                var name = value.name;
                var task = value.task;
                $("tbody").append(
                    "<tr><td>" + id + "</td><td>" + strength + "</td><td>" + agility + "</td><td>" + intellect + "</td><td>" + name + "</td><td>" + task + "</td></tr>"
                )
            })
        },
        error: function (data) {
            console.log("error");
            console.log(data)
        }
    })
}, 3000);